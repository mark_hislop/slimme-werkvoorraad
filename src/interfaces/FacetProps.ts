export interface FacetProps {
    fields: [{
        count: string,
        labelt: string
    }],
    name: string
}