export interface RecordsProps {
    items?: Array<JSON>,
    itemsCount?: Number,
    requestNumber?: Number,
    success?: Boolean,
    totalResults?: Number
}


  