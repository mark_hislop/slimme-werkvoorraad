import { TextField } from '@mui/material';
import axios from 'axios';
import { useState } from 'react';
import { useLocation } from 'react-router-dom';
import Button from '../shared/Button';
import styles from './styles/AddPage.module.css';

const AddPage = () => {
	const { state } = useLocation();
	const [title, setTitle] = useState({ value: state.title, error: '' });
	const [description, setDescription] = useState(state.description);

	const sendItem = () => {
		/* Validate metadata */
		if (title.value === '') {
			setTitle({ value: '', error: 'Veld is leeg' });
			return;
		}

		/* Send item to SURFsharekit */
		const body = {
			title: title.value,
			description,
			type: 'LearningObject',
			institute: '97581db2-0a84-4f86-b804-49a9c575b0f9',
			link: state.link,
			files: [
				{
					fileId: 'f6822cf8-85c3-4f1e-870b-d8d35937365a',
					title: 'test.pdf',
					access: 'closedAccess',
				},
			],
		};

		axios
			.post(`${process.env.REACT_APP_API_URL}api/v1/records`, body)
			.then((res) => window.alert(JSON.stringify(res.data, null, 4)))
			.catch((err) => window.alert(JSON.stringify(err.response.data, null, 4)));
	};

	return (
		<div className={styles.container}>
			<a href={state.link} target="blank" className={styles.anker}>
				Link naar item
			</a>
			<TextField
				defaultValue={title.value}
				label="Titel"
				onChange={(e) => setTitle({ value: e.target.value, error: '' })}
				error={title.error.length === 0 ? false : true}
				helperText={title.error}
			/>
			<TextField
				defaultValue={description}
				label="Samenvatting"
				onChange={(e) => setDescription(e.target.value)}
				multiline
			/>
			<TextField defaultValue="Leermateriaal" label="RepoType" disabled />
			<TextField defaultValue="SURF edusources test" label="InstituteID" disabled />
			<Button label="Verstuur naar SURFsharekit" onClick={sendItem}></Button>
		</div>
	);
};

export default AddPage;
