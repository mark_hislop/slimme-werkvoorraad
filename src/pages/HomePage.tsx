import axios from 'axios';
import { useState } from 'react';

import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';

import ResultContainer from '../containers/ResultContainer';
import styles from './styles/HomePage.module.css';
import FilterContainer from '../containers/FilterContainer';
import SearchContainer from '../containers/SearchContainer';
import Header from '../components/Header';
import RecordSkeleton from '../components/RecordSkeleton';
import Jumbotron from '../components/Jumbotron';

const HomePage = () => {
	const [keywords, setKeywords] = useState({
		value: '',
		error: '',
	});
	const [records, setRecords] = useState([]);
	const [recordsTotal, setRecordsTotal] = useState(undefined);
	const [page, setPage] = useState(1);
	const [isLoading, setIsLoading] = useState(false);
	const [theme, setTheme] = useState('');

	const changePage = (e: any, selectedPage: number) => {
		window.scrollTo(0, 0);
		setPage(selectedPage);
	};

	const urlBuilder = () => {
		let url = '';
		/* Check if a "Theme" Value is Set */
		if (theme === '') {
			url = `${process.env.REACT_APP_API_URL}api/v1/records?keywords=${keywords.value}`;
		} else {
			url = `${process.env.REACT_APP_API_URL}api/v1/collections?keywords=${keywords.value}&theme=${theme}`;
		}

		return url;
	};

	const getRecords = () => {
		/* Check If Keyword is Empty */
		if (keywords.value === '') {
			setKeywords({ value: '', error: 'Zoektermveld is leeg' });
			return;
		}

		setIsLoading(true);

		/* Retrieve records */
		axios({
			method: 'get',
			url: urlBuilder(),
		})
			.then(({ data }) => {
				console.log(data); // Log
				setPage(1);
				setRecords(data.items);
				setRecordsTotal(data.items.length);
				setIsLoading(false);
			})
			.catch((error) => {
				window.alert(error.message);
			});
	};

	return (
		<main>
			<Header />
			<Jumbotron />
			<div className={styles.container}>
				<div className={styles.search_container}>
					<SearchContainer
						keywords={keywords}
						inputOnChange={(e: any) =>
							setKeywords({ error: '', value: e.target.value })
						}
						buttonOnClick={getRecords}
						theme={theme}
						themeOnChange={(e: any) => setTheme(e.target.value)}
					/>
				</div>
				<aside className={styles.filter_container}>
					{isLoading ? <Skeleton count={4} /> : ''}
					<FilterContainer isLoading={isLoading} />
				</aside>
				<div className={styles.result_container}>
					{isLoading ? <RecordSkeleton /> : ''}
					<ResultContainer
						records={records.slice(page * 10 - 10, page * 10 - 1)}
						recordsTotal={recordsTotal}
						page={page}
						paginationOnChange={changePage}
						isLoading={isLoading}
					/>
				</div>
			</div>
		</main>
	);
};

export default HomePage;
