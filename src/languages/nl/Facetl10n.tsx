const Facet = {
    YEAR: "Jaar",
    DATA_PROVIDER: "Instituut",
    COUNTRY: "Land",
    LANGUAGE: "Taal",
    TYPE: "Type",
    REUSABILITY: "Herbruikbaarheid",
    IMAGE_SIZE: "Afbeeldinggrootte",
    SOUND_DURATION: "Geluidsduur"
}
export default Facet

