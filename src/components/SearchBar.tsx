import { ChangeEventHandler, MouseEventHandler, useEffect, useState } from 'react';
import axios from 'axios';
import { TextField } from '@mui/material';
import styles from './styles/SearchBar.module.css';
import InputSelect from '../shared/InputSelect';
import Button from '../shared/Button';

interface SearchBarInterface {
	keywords: any;
	inputOnChange: ChangeEventHandler;
	buttonOnClick: MouseEventHandler;
	theme: string;
	themeOnChange: any;
}

const SearchBar = ({
	keywords,
	inputOnChange,
	buttonOnClick,
	theme,
	themeOnChange,
}: SearchBarInterface) => {
	const [themes, setThemes] = useState([]);

	const getThemes = () => {
		axios({
			method: 'get',
			url: `${process.env.REACT_APP_API_URL}api/v1/collections/format`,
		})
			.then(({ data }) => {
				setThemes(data);
			})
			.catch((response: any) => {
				window.alert(response.message);
			});
	};

	useEffect(() => {
		getThemes();
	}, []);

	return (
		<div className={styles.container}>
			<div className={styles.searchbar_label}>Trefwoord</div>
			<div className={styles.searchbar}>
				<TextField
					inputProps={{ 'data-testid': 'keywords-input' }}
					type="text"
					variant="outlined"
					value={keywords.value}
					onChange={inputOnChange}
					error={keywords.error.length === 0 ? false : true}
					helperText={keywords.error}
					placeholder="Zoek op materiaal"
					style={{ width: '100%' }}
				/>
				<InputSelect standardValue={theme} onChange={themeOnChange} options={themes} />
				<Button label="Zoeken" onClick={buttonOnClick} />
			</div>
		</div>
	);
};

export default SearchBar;
