import { TextField } from '@mui/material';
import { useState } from 'react';
import Button from '../shared/Button';
import styles from './styles/FilterYear.module.css';

const FilterYear = () => {
	const [startYear, setStartYear] = useState({ value: 0, error: '' });
	const [endYear, setEndYear] = useState({ value: 2022, error: '' });
	const currentYear = new Date().getFullYear();

	const validateInput = () => {
		/* Check if startyear is between 0 and current year */
		if (!(startYear.value >= 0 && startYear.value <= currentYear)) {
			setStartYear({
				value: startYear.value,
				error: `Jaar ligt niet tussen 0 en ${currentYear}`,
			});
			return;
		}

		/* Check if endyear is between 0 and current year */
		if (!(endYear.value! >= 0 && endYear.value! <= currentYear)) {
			setEndYear({
				value: endYear.value,
				error: `Jaar ligt niet tussen 0 en ${currentYear}`,
			});
			return;
		}

		/* Check if startyear is lower or equal to endyear */
		if (!(startYear.value <= endYear.value)) {
			setEndYear({
				value: endYear.value,
				error: `Startjaar ligt boven eindjaar`,
			});
			return;
		}
	};

	return (
		<div className={styles.container}>
			<div className={styles.title}>Jaar </div>
			<div className={styles.input_container}>
				<TextField
					label="Beginjaar"
					type="number"
					variant="outlined"
					value={startYear.value}
					onChange={(e) => {
						setStartYear({ value: parseInt(e.target.value), error: '' });
						setEndYear({ value: endYear.value, error: '' });
					}}
					error={startYear.error.length === 0 ? false : true}
					helperText={startYear.error}
					inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
				/>
				<TextField
					label="Eindjaar"
					type="number"
					variant="outlined"
					value={endYear.value}
					onChange={(e) => {
						setStartYear({ value: startYear.value, error: '' });
						setEndYear({ value: parseInt(e.target.value), error: '' });
					}}
					error={endYear.error.length === 0 ? false : true}
					helperText={endYear.error}
					inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
				/>
			</div>
			<Button label="Toepassen" onClick={validateInput} />
		</div>
	);
};

export default FilterYear;
