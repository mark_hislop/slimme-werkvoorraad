import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';

import styles from './styles/Record.module.css';

const RecordSkeleton = () => {
	return (
		<div>
			<Skeleton />
			<br />
			{[1, 2].map((index) => (
				<article className={styles.container} key={index}>
					<div className={styles.preview} style={{ backgroundColor: '#F6F6F6' }}></div>
					<div className={styles.metadata} style={{ width: '400px' }}>
						<h4 className={styles.name}>
							<Skeleton />
						</h4>
						<div className={styles.description}>
							<Skeleton />
						</div>
						<div className={styles.link}>
							<Skeleton />
						</div>
					</div>
				</article>
			))}
		</div>
	);
};
export default RecordSkeleton;
