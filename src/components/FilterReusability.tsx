import { FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import styles from './styles/FilterReusability.module.css';

const ReusabilityFilter = () => {
	return (
		<div>
			<div className={styles.title}>Herbruikbaarheid</div>
			<FormGroup>
				<FormControlLabel
					control={<Checkbox size="small" disabled checked />}
					label="Open Access"
					sx={{}}
				/>
				<FormControlLabel
					control={<Checkbox size="small" disabled />}
					label="Restricted Access"
				/>
				<FormControlLabel
					control={<Checkbox size="small" disabled />}
					label="Closed Access"
				/>
			</FormGroup>
		</div>
	);
};

export default ReusabilityFilter;
