import { fireEvent, render } from "@testing-library/react";
import SearchBar from "../SearchBar";

describe(SearchBar, () => {
    it("Themes are loaded", async () => {
        const {getByTestId, getByLabelText, getByText} = render(
        <SearchBar 
            keywords={{value: "test", error: ""}} 
            inputOnChange={() => {}}
            buttonOnClick={() => {}}
            theme=""
            themeOnChange={() => {}}
        
        />)
        const themes = getByTestId("themes")
        fireEvent.mouseDown(themes)
        fireEvent.click(getByText("art"));
        fireEvent.keyDown(document.activeElement, {
            key: "Escape",
            code: "Escape"
          });
        await waitForElementToBeRemoved(getByText(""));
        expect(getByLabelText("Thema")).toHaveTextContent(
            "Art"
        );
    });
})