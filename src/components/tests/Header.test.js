import { render } from "@testing-library/react";
import Header from "../Header";

describe(Header, () => {
    it("Header title displays", () => {
        const {getByText} = render(<Header />)
        const title = getByText("Slimme Werkvoorraad").textContent
        expect(title).toEqual("Slimme Werkvoorraad")
    });
})