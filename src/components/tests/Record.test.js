import { getByAltText, render } from "@testing-library/react";
import Record from "../Record";
import { BrowserRouter, Router } from "react-router-dom";

describe(Record, () => {
    it("Record displays correct metadata", () => {
        const {getByText, getByAltText} = render(
        <BrowserRouter>
            <Record 
                id="1"
                file="www.test.nl"
                title="Testtitle"
                description="Testdescription"
                link="www.google.nl"
                provider="Testprovider"
                filetype="text"
                preview="https://cdn.pixabay.com/photo/2013/07/12/17/47/test-pattern-152459_960_720.png"
                repository="Testrepository"
            />
        </BrowserRouter>)
        const title = getByText("Testtitle").textContent
        const description = getByText("Testdescription").textContent
        const provider = getByText("Testprovider").textContent
        const filetype = getByText("text").textContent
        const repository = getByText("Testrepository").textContent
        const image = getByAltText("preview")
        expect(title).toEqual("Testtitle")
        expect(description).toEqual("Testdescription")
        expect(provider).toEqual("Testprovider")
        expect(filetype).toEqual("text")
        expect(repository).toEqual("Testrepository")
        expect(image.src).toContain("https://cdn.pixabay.com/photo/2013/07/12/17/47/test-pattern-152459_960_720.png")
    });
})