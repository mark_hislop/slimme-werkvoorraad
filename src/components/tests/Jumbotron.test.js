import { render } from "@testing-library/react";
import Jumbotron from "../Jumbotron";

describe(Jumbotron, () => {
    it("Title displays", () => {
        const {getByText} = render(<Jumbotron />)
        const title = getByText("Hoofdtitel van de website").textContent
        expect(title).toEqual("Hoofdtitel van de website")
    });

    it("Subtitle displays", () => {
        const {getByText} = render(<Jumbotron />)
        const title = getByText("Subtitel met meer tekst").textContent
        expect(title).toEqual("Subtitel met meer tekst")
    });
})