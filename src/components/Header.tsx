import styles from './styles/Header.module.css';

function Header() {
	return (
		<header className={styles.container}>
			<a href="/">
				<div className={styles.logo}>
					<img src="../images/surf_logo.png" alt="Logo van SURF" />
					<div className={styles.label}>Slimme Werkvoorraad</div>
				</div>
			</a>
		</header>
	);
}

export default Header;
