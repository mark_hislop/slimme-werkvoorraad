import AccountBoxIcon from '@mui/icons-material/AccountBox';
import ArticleIcon from '@mui/icons-material/Article';
import Inventory2RoundedIcon from '@mui/icons-material/Inventory2Rounded';
import { useNavigate } from 'react-router-dom';
import Button from '../shared/Button';

import styles from './styles/Record.module.css';

interface RecordInterface {
	id: string;
	title: string;
	description: string;
	link: string;
	provider: string;
	filetype: string;
	preview: string;
	repository: string;
}

const Record = ({
	id,
	title,
	description,
	link,
	provider,
	filetype,
	preview,
	repository,
}: RecordInterface) => {
	const navigate = useNavigate();

	return (
		<article className={styles.container}>
			<div className={styles.preview}>
				{preview === '' ? (
					<div style={{ height: 'inherit', backgroundColor: '#F6F6F6' }} />
				) : (
					<img src={preview} alt="preview" />
				)}
			</div>
			<div className={styles.metadata}>
				<h4 className={styles.name}>{title}</h4>
				<div className={styles.description}>{description}</div>
				<a className={styles.link} href={link} target="blank">
					Meer informatie
				</a>

				<Button
					label="Toevoegen aan edusources"
					style={{ width: '300px' }}
					onClick={() =>
						navigate('/add', {
							state: { title, description, link },
						})
					}
				/>

				<div className={styles.icons}>
					<div>
						<Inventory2RoundedIcon fontSize="small" />
						{repository}
					</div>
					<div>
						<AccountBoxIcon fontSize="small" />
						{provider}
					</div>
					<div>
						<ArticleIcon fontSize="small" />
						{filetype.toLowerCase()}
					</div>
				</div>
			</div>
		</article>
	);
};

export default Record;
