import styles from './styles/Jumbotron.module.css';

const Jumbotron = () => {
	return (
		<div className={styles.container}>
			<div className={styles.jumbotron}>
				<div className={styles.title}>
					<h1>Welkom bij de Slimme Werkvoorraad</h1>
					<h2 style={{ fontWeight: 'normal' }}>Subtitel met meer tekst</h2>
				</div>
			</div>
		</div>
	);
};

export default Jumbotron;
