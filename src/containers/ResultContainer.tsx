import Pagination from '@mui/material/Pagination/Pagination';

import styles from './styles/ResultContainer.module.css';
import Record from '../components/Record';

interface ResultContainerInterface {
	records: any;
	recordsTotal: any;
	page: number;
	paginationOnChange: (e: any, selectedPage: number) => void;
	isLoading: Boolean;
}

const ResultContainer = ({
	records,
	recordsTotal,
	page,
	paginationOnChange,
	isLoading,
}: ResultContainerInterface) => {
	return (
		<div style={{ display: isLoading ? 'none' : 'block' }}>
			<div className={styles.records_total}>
				{recordsTotal === undefined
					? ''
					: recordsTotal.toLocaleString() + ' zoekresultaten gevonden'}
			</div>
			{records.map((record: any) => (
				<Record key={record.id} {...record} />
			))}
			<div className={styles.pagination}>
				{recordsTotal === undefined ? (
					''
				) : (
					<Pagination
						page={page}
						count={Math.ceil(recordsTotal / 10)}
						variant="outlined"
						shape="rounded"
						onChange={paginationOnChange}
					/>
				)}
			</div>
		</div>
	);
};

export default ResultContainer;
