import { ChangeEventHandler, MouseEventHandler } from 'react';

import SearchBar from '../components/SearchBar';

interface SearchContainerInterface {
	keywords: object;
	inputOnChange: ChangeEventHandler;
	buttonOnClick: MouseEventHandler;
	theme: string;
	themeOnChange: ChangeEventHandler;
}

const SearchContainer = ({
	keywords,
	inputOnChange,
	buttonOnClick,
	theme,
	themeOnChange,
}: SearchContainerInterface) => {
	return (
		<SearchBar
			keywords={keywords}
			inputOnChange={inputOnChange}
			buttonOnClick={buttonOnClick}
			theme={theme}
			themeOnChange={themeOnChange}
		/>
	);
};

export default SearchContainer;
