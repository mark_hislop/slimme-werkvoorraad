import styles from './styles/FilterContainer.module.css';
import FilterReusability from '../components/FilterReusability';
import FilterYear from '../components/FilterYear';

interface FilterContainerInterface {
	isLoading: Boolean;
}

const FilterContainer = ({ isLoading }: FilterContainerInterface) => {
	return (
		<div>
			{isLoading ? (
				''
			) : (
				<aside className={styles.container}>
					<FilterReusability />
					<FilterYear />
				</aside>
			)}
		</div>
	);
};

export default FilterContainer;
