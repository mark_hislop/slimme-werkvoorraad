import { ChangeEventHandler } from 'react';

import styles from './styles/InputText.module.css';

interface InputTextInterface {
	onChange: ChangeEventHandler;
	style?: Object;
	placeholder: string;
}

const InputText = (props: InputTextInterface) => {
	return <input type="text" className={styles.input} autoComplete="1" {...props} />;
};

export default InputText;
