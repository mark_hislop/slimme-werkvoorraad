import { Box, FormControl, InputLabel, MenuItem, Select } from '@mui/material';

interface SearchBarInterface {
	standardValue: string;
	onChange: any;
	options: any;
}

const InputSelect = ({ standardValue, onChange, options }: SearchBarInterface) => {
	return (
		<Box sx={{ minWidth: 120 }}>
			<FormControl fullWidth>
				<InputLabel id="demo-simple-select-label">Vakgebied</InputLabel>
				<Select value={standardValue} onChange={onChange} label="Thema">
					<MenuItem value="">Geen</MenuItem>
					{options.map((option: any) => (
						<MenuItem key={option.key} value={option.value}>
							{option.labelNL}
						</MenuItem>
					))}
				</Select>
			</FormControl>
		</Box>
	);
};

export default InputSelect;
