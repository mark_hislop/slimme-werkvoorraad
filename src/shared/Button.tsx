import { MouseEventHandler } from 'react';
import styles from './styles/Button.module.css';

interface InputButtonInterface {
	label: string;
	style?: Object;
	onClick: MouseEventHandler;
}

const Button = ({ label, style, onClick }: InputButtonInterface) => {
	return (
		<button type="submit" className={styles.button} onClick={onClick} style={style}>
			{label}
		</button>
	);
};

export default Button;
